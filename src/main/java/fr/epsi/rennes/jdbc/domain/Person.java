package fr.epsi.rennes.jdbc.domain;

public class Person {

	private int id;
	private String fullName;
	private String twitter;
	
	public Person() {
	}
	
	public Person( String fullName, String twitter ) {
		this.fullName = fullName;
		this.twitter = twitter;
	}
	
	public Person( int id, String fullName, String twitter ) {
		this.id = id;
		this.fullName = fullName;
		this.twitter = twitter;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId( int id ) {
		this.id = id;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName( String fullName ) {
		this.fullName = fullName;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Person{" );
		sb.append( "id=" ).append( id );
		sb.append( ", fullName='" ).append( fullName ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}

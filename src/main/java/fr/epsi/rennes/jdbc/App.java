package fr.epsi.rennes.jdbc;

import fr.epsi.rennes.jdbc.domain.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class App {
	
	public static void main( String[] args ) throws SQLException {
		
		ResourceBundle bundle = ResourceBundle.getBundle( "db" );
		String dbUrl = bundle.getString( "database.url" );
		String dbLogin = bundle.getString( "database.login" );
		String dbPwd = bundle.getString( "database.password" );
		
		try( Connection connection = DriverManager.getConnection( dbUrl, dbLogin, dbPwd );
			 Statement st = connection.createStatement()) {
			
			Person person = new Person( "E. Sylla", "esylla" );
			st.executeUpdate( "INSERT INTO t_person (nom_complet, twitter) " +
					"			VALUES ('"+person.getFullName()+"', '"+person.getTwitter()+"')" );
			
			
			List<Person> personsList = new ArrayList<>();
			try( ResultSet rs = st.executeQuery( "SELECT * FROM t_person" )) {
				while(rs.next()) {
					Person personItem = new Person(rs.getInt( "id" ),
							rs.getString( "nom_complet" ),
							rs.getString( "twitter" )
					);
					personsList.add( personItem );
				}
			}
			
			for ( Person item : personsList ) {
				System.out.println(item);
			}
		}
		
	}
}
